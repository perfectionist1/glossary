<?php
include_once("vendor/autoload.php");
include_once('lib/app.php');

use Example\Crud\Profile;

$profile = new Profile();
$profiles = $profile->all();
    //print_r($profiles);

?>


<!DOCTYPE html>

<html>
    <head lang="en">
        <meta charset="UTF-8">
        <title>Glossary</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    
    <body>
            <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                        <h1 class="text-center">
                            Glossary
                            <a href="add_new.html" class="btn btn-primary pull-right">
                                <i class="glyphicon glyphicon-plus"></i> Add New</a>
                        </h1>
                    <table class="table table-responsive table-striped table-bordered table-hover">
                        <tr>
                            <th class="text-center" width="7%">Sl No.</th>
                            <th class="text-center" width="15%">Abbreviation</th>
                            <th class="text-center" width="60%">Stand for</th>
                            <th class="text-center" width="20%">Options</th>
                        </tr>
                        <?php
                        foreach ($profiles as $profile)
                        {
                        ?>
                        <tr>
                            <td><?php echo $profile['id']; ?></td>
                            <td><a href="#"><?php echo $profile['abbreviation']; ?></a></td>
                            <td><?php echo $profile['elaboration']; ?></td>
                            <td class="text-center">
                                <a href="#" title="Details"><i class="glyphicon glyphicon-dashboard"></i>
                                </a>
                                <a href="#" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="#" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                    <nav class="text-center">
                        <ul class="pagination">
                            <li class="disabled">
                                <a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li class="active">
                                <a href="#">1 <span class="sr-only">(current)</span></a>
                            </li>
                            <li class=""><a href="#">2 <span class="sr-only">(current)</span></a></li>
                            <li class=""><a href="#">3 <span class="sr-only">(current)</span></a></li>
                            <li class=""><a href="#">4 <span class="sr-only">(current)</span></a></li>
                            <li class="disabled">
                                <a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </body>
</html>