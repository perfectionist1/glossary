<?php

namespace Example\Crud;

use Example\Utility;

class Profile {

    public $email = '';
    public $fullname = '';
    public $data = array();
    private $conn = "";

    function __construct() {
        $this->conn = new \PDO('mysql:host=localhost;dbname=crud', 'root', '');
        $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function all() {

        try {
            $query = "SELECT * FROM profiles";

            $_result = $this->conn->query($query);

            foreach ($_result as $row) {
                $this->data[] = $row;
            }

            return $this->data;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return $this->data;
        }

        
    }

    public function create() {

        $conn = mysql_connect('localhost', 'root', '');
        mysql_select_db('crud');


        $query = "INSERT INTO `crud`.`profiles` (`id`, `email`, `fullname`) VALUES (NULL, '" . $_POST['email'] . "', '" . $_POST['fullname'] . "')";
        //echo  $query;
        if (mysql_query($query)) {
            $_SESSION['message'] = "Successfully added.";
            echo "success";
        } else {
            $_SESSION['message'] = "Fail to add.";
            echo mysql_error();
        }

        //Utility::redirect();
    }

public function get($id = null) {

    //echo $id; //input directly from user
    try {

        $data = $this->conn->query('SELECT * FROM '
                . 'profiles WHERE id =' . 
                $this->conn->quote($id));
        
        $stmt = $this->conn->prepare('SELECT * FROM profiles WHERE id = :id');
        $stmt->execute(array('id' => $id));
        $row = $stmt->fetch();

        return $row;


        
    } catch (PDOException $e) {
        echo 'ERROR: ' . $e->getMessage();
    }
}

    public function store() {

        $query = "UPDATE `crud`.`profiles` SET `email` = '" . $_POST['email'] . "', `fullname` = '" . $_POST['fullname'] . "' WHERE `profiles`.`id` =" . $_POST['id'];
        mysql_query($query);

        redirect();
    }

    function delete() {

        $query = "DELETE FROM `crud`.`profiles` WHERE `profiles`.`id` = " . $_GET['id'];
        mysql_query($query);

        redirect('http://yahoo.com');
    }

}
