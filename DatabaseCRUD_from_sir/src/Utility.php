<?php

namespace Example;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Utilility
 *
 * @author BITM Trainer - 302
 */
class Utility {
    
    function debug($data){
        echo "<pre>";
        print_r($data);
        //var_dump($data);
        echo "</pre>";
    }

    static function redirect($url = 'index.php'){
       header('location:'.$url);
    }
    
}
