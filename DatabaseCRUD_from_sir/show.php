<?php
include_once("vendor/autoload.php");
include_once('lib/app.php');

use Example\Crud\Profile;

$profile = new Profile();
$data = $profile->get($_GET['id']);
?>

<html>
    <head>
        <title>Understanding CRUD</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
<h1>Your Details</h1>

<dl>
    <dt>Full Name</dt>
    <dd><?php 
        if(array_key_exists('fullname',$data) && !empty($data['fullname'])){   
            echo $data['fullname'];
        }else{
            echo "Not Provided";
        }
       ?></dd>
    
    <dt>Email</dt>
    <dd><?php 
    if(array_key_exists('email',$data) && !empty($data['email'])){   
            echo $data['email'];
        }else{
            echo "Not Provided";
        }
    ?></dd>
    
</dl>

<nav>
    <li><a href="index.php">List</a></li>
    <li><a href="create.html">Create</a></li>    
    <li><a href="edit.php?id=<?php echo $_GET['id']?>">Edit</a></li>    
</nav>

    </body>
</html>